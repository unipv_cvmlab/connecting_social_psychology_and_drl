# Connecting Social Psychology and Deep Reinforcement Learning: A Probabilistic Predictor on the Intention to Do Home-Based Physical Activity after Message Exposure

### Patrizia Catellani, Valentina Carfora, Marco Piastra

## Supplementary Material

This repository contains supplementary material for the article above.

1. File `LOCKDOWN.csv`
   Comma-separated text file including raw experimental data.
   
2. File `LOCKDOWN_AUTO_n6c0_n6c0 FRONTIERS.xdsl`  
   XML file containing the complete definition of the Dynamic Bayesian Network (DBN) automatically elicited and trained from the raw data above  
   (see the article for further explanation).
   
> NOTE: the `.xsdl` file is in the format used in Genie Modeler by BayesFusion, LLC  
> (see https://www.bayesfusion.com/genie/)

